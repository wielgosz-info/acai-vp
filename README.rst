==============================================
Video Prediction models implemented in PyTorch
==============================================


Description
===========

A longer description of your project goes here...


Working with Docker
===================


Build
-----

.. code-block:: sh

    cd acai-vp
    git pull
    docker build -t wielgoszinfo/acai-vp --build-arg UID=`id -u` --build-arg GID=`id -g` .


Run
---

To run the container (execute acai_vp experiment),
first we need to ensure the old instances are not running
and/or remove them:

.. code-block:: sh

    docker container ls --filter name=acai_vp
    docker container rm acai_vp

Run the container:

.. code-block:: sh

    docker run \
        --gpus all \
        --mount 'type=volume,src=data,dst=/data,readonly' \
        --mount 'type=volume,src=acai-vp-out,dst=/out' \
        --name acai_vp \
        --shm-size 16GB \
        --icp private
        -itd wielgoszinfo/acai-vp

Copy config into the container:

.. code-block:: sh

    docker cp YOUR/CONFIG/FILE.yaml acai_vp:/acai-vp-config.yaml

Attach to container and execute experiment:

.. code-block:: sh

    docker container attach acai_vp
    # inside container
    acai_vp
    # which is equivalent to
    python3.6 -m acai_vp \
        -vv \
        --data-dir /data \
        --out-dir /out \
        --config-file /acai-vp-config.yaml \
        --no-visdom

Detach with:

.. code-block::

    CTRL+P CTRL+Q

Just remember not to kill it (e.g. ``CTRL+C``)!

Connect to running container
----------------------------

You can attach and check progress by running (and have patience,
it will display something only after current batch completes):

.. code-block:: sh

    docker container attach acai_vp

Get the output
--------------

By default the output files will be stored in the `acai-vp-out` volume.
To get them out, use `docker cp <https://docs.docker.com/engine/reference/commandline/cp/>`_.
Sorry ;)

Note
====

This project has been set up using PyScaffold 3.2.3. For details and usage
information on PyScaffold see https://pyscaffold.org/.
