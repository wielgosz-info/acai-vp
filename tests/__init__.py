import os
import torch

from PIL import Image


FIXTURE_DIR = os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    'test_files',
)


def rgb_to_byte_tensor(path):
    im = Image.open(os.path.join(
        FIXTURE_DIR, path))
    return im, torch.ByteTensor(torch.ByteStorage.from_buffer(
        im.tobytes())).view((im.size[1], im.size[0], 3))
