import torch
import numpy
import pytest

from acai_vp.utils.operations.covariance_matrix import covariance_matrix


def test_covariance_matrix():
    x = torch.rand((100,), dtype=torch.float32)
    y = torch.rand((100,), dtype=torch.float32)

    # numpy baseline
    np_cov = torch.from_numpy(
        numpy.cov(x.numpy(), y.numpy())).float()

    cov = covariance_matrix(x, y)

    assert torch.allclose(cov, np_cov)
