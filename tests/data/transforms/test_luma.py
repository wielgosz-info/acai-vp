import os
import pytest
import torch

from tests import rgb_to_byte_tensor
from acai_vp.data.transforms.luma import RGBToLuma


def test_RGBToLuma():
    orig_im, torch_orig_im = rgb_to_byte_tensor(
        "trang-doan-acai-bowl-3118421.jpg")  # HxWxC

    orig_luma = orig_im.convert('L')
    torch_orig_luma = torch.ByteTensor(torch.ByteStorage.from_buffer(
        orig_luma.tobytes())).view((orig_luma.size[1], orig_luma.size[0], 1))  # HxWxC

    converter = RGBToLuma()
    torch_acai_luma = converter(torch_orig_im)  # HxWxC

    # allow for small deviations from Pillow 'ideal'
    assert torch.allclose(torch_acai_luma, torch_orig_luma, atol=1)

    norm_acai_luma = converter(torch_orig_im.float().div(255.0))
    norm_orig_luma = torch_orig_luma.float().div(255.0)

    assert torch.allclose(norm_acai_luma, norm_orig_luma, atol=1e-2)
