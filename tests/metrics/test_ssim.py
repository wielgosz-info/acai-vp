import pytest
import torch

from tests import rgb_to_byte_tensor
from acai_vp.metrics.ssim import *
from acai_vp.data.transforms.luma import RGBToLuma


def test_per_square_window_SSIM():
    orig_im, torch_orig_im = rgb_to_byte_tensor(
        "trang-doan-acai-bowl-3118421.jpg")

    norm_im = torch_orig_im.float().div(255.0)

    N = 8

    converter = RGBToLuma()
    luma = converter(norm_im.view(
        (1, 1, *norm_im.shape)))[0, 0, :N, :N, 0].reshape((1, N*N, 1))  # Bx(C*N*N)xL

    assert torch.allclose(per_square_window_SSIM(luma, luma),
                          torch.tensor([[[1.0]]], dtype=torch.float32),
                          atol=1e-8), "SSIM for self should be 1"

    uniform_noise = torch.rand_like(luma, dtype=torch.float32)

    assert torch.allclose(per_square_window_SSIM(uniform_noise, luma),
                          torch.tensor([[[0.0]]], dtype=torch.float32),
                          atol=1e-1), "SSIM for noise should be close to 0"


def test_block_SSIM():
    orig_im, torch_orig_im = rgb_to_byte_tensor(
        "trang-doan-acai-bowl-3118421.jpg")

    norm_im = torch_orig_im.float().div(255.0).view((1, 1, *torch_orig_im.shape))

    mean_ssim, per_clip_mean_ssim = block_SSIM(norm_im, norm_im)

    assert torch.allclose(mean_ssim,
                          torch.tensor(1.0, dtype=torch.float32),
                          atol=1e-8), "Mean SSIM for self should be 1"
    assert torch.allclose(per_clip_mean_ssim,
                          torch.tensor([1.0], dtype=torch.float32),
                          atol=1e-8), "SSIM for self should be 1"

    uniform_noise = torch.rand_like(norm_im, dtype=torch.float32)

    mean_ssim, per_clip_mean_ssim = block_SSIM(uniform_noise, norm_im)

    assert torch.allclose(mean_ssim,
                          torch.tensor(0.0, dtype=torch.float32),
                          atol=1e-1), "Mean SSIM for noise should be close to 0"

    assert torch.allclose(per_clip_mean_ssim,
                          torch.tensor([0.0], dtype=torch.float32),
                          atol=1e-1), "SSIM for noise should be close to 0"


def test_block_SSIM_on_batch():
    orig_im, torch_orig_im = rgb_to_byte_tensor(
        "trang-doan-acai-bowl-3118421.jpg")

    norm_im = torch_orig_im.float().div(255.0).view((1, 1, *torch_orig_im.shape))
    uniform_noise = torch.rand_like(norm_im, dtype=torch.float32)

    H = 224
    W = 224

    batched_input = torch.stack(
        (norm_im[0, :, :H, :W, :], norm_im[0, :, H:2*H, W:2*W, :]))
    batched_target = torch.stack(
        (norm_im[0, :, :H, :W, :], uniform_noise[0, :, H:2*H, W:2*W, :]))

    mean_ssim, per_clip_mean_ssim = block_SSIM(batched_input, batched_target)

    assert mean_ssim.shape == tuple(), "Mean SSIM should be a singleton"
    assert per_clip_mean_ssim.shape == (
        2,), "Per-clip SSIM should have length equal to batch size"

    assert torch.allclose(mean_ssim,
                          torch.tensor(0.5, dtype=torch.float32),
                          atol=1e-1), "Mean SSIM for self & noise should be close to 0.5"
    assert torch.allclose(per_clip_mean_ssim[0],
                          torch.tensor(1.0, dtype=torch.float32),
                          atol=1e-8), "SSIM for self should be 1"
    assert torch.allclose(per_clip_mean_ssim[1],
                          torch.tensor(0.0, dtype=torch.float32),
                          atol=1e-1), "SSIM for noise should be close to 0"


def test_block_SSIM_on_sequence():
    orig_im, torch_orig_im = rgb_to_byte_tensor(
        "trang-doan-acai-bowl-3118421.jpg")

    norm_im = torch_orig_im.float().div(255.0).view((1, 1, *torch_orig_im.shape))
    uniform_noise = torch.rand_like(norm_im, dtype=torch.float32)

    H = 224
    W = 224

    batched_input = torch.stack(
        (norm_im[:, 0, :H, :W, :], norm_im[:, 0, H:2*H, W:2*W, :]), dim=1)
    batched_target = torch.stack(
        (norm_im[:, 0, :H, :W, :], uniform_noise[:, 0, H:2*H, W:2*W, :]), dim=1)

    mean_ssim, per_clip_mean_ssim = block_SSIM(batched_input, batched_target)

    assert mean_ssim.shape == tuple(), "Mean SSIM should be a singleton"
    assert per_clip_mean_ssim.shape == (
        1,), "Per-clip SSIM should have length equal to batch size"

    assert torch.allclose(mean_ssim,
                          torch.tensor(0.5, dtype=torch.float32),
                          atol=1e-1), "Mean SSIM for self & noise should be close to 0.5"
    assert torch.allclose(per_clip_mean_ssim,
                          torch.tensor([0.5], dtype=torch.float32),
                          atol=1e-1), "SSIM for self & noise should be close to 0.5"


def test_per_gaussian_window_SSIM():
    orig_im, torch_orig_im = rgb_to_byte_tensor(
        "trang-doan-acai-bowl-3118421.jpg")

    norm_im = torch_orig_im.float().div(255.0)

    N = 11

    converter = RGBToLuma()
    luma = converter(norm_im.view(
        (1, 1, *norm_im.shape)))[0, 0, :N, :N, 0].view((1, 1, N, N))  # BxCxWxH

    assert torch.allclose(per_gaussian_window_SSIM(luma, luma),
                          torch.tensor([[[1.0]]], dtype=torch.float32),
                          atol=1e-8), "SSIM for self should be 1"

    uniform_noise = torch.rand_like(luma, dtype=torch.float32)

    assert torch.allclose(per_gaussian_window_SSIM(uniform_noise, luma),
                          torch.tensor([[[0.0]]], dtype=torch.float32),
                          atol=1e-1), "SSIM for noise should be close to 0"


def test_SSIM():
    orig_im, torch_orig_im = rgb_to_byte_tensor(
        "trang-doan-acai-bowl-3118421.jpg")

    norm_im = torch_orig_im.float().div(255.0).view((1, 1, *torch_orig_im.shape))

    mean_ssim, per_clip_mean_ssim = SSIM(norm_im, norm_im)

    assert torch.allclose(mean_ssim,
                          torch.tensor(1.0, dtype=torch.float32),
                          atol=1e-8), "Mean SSIM for self should be 1"
    assert torch.allclose(per_clip_mean_ssim,
                          torch.tensor([1.0], dtype=torch.float32),
                          atol=1e-8), "SSIM for self should be 1"

    uniform_noise = torch.rand_like(norm_im, dtype=torch.float32)

    mean_ssim, per_clip_mean_ssim = SSIM(uniform_noise, norm_im)

    assert torch.allclose(mean_ssim,
                          torch.tensor(0.0, dtype=torch.float32),
                          atol=1e-1), "Mean SSIM for noise should be close to 0"

    assert torch.allclose(per_clip_mean_ssim,
                          torch.tensor([0.0], dtype=torch.float32),
                          atol=1e-1), "SSIM for noise should be close to 0"


def test_SSIM_on_batch():
    orig_im, torch_orig_im = rgb_to_byte_tensor(
        "trang-doan-acai-bowl-3118421.jpg")

    norm_im = torch_orig_im.float().div(255.0).view((1, 1, *torch_orig_im.shape))
    uniform_noise = torch.rand_like(norm_im, dtype=torch.float32)

    H = 224
    W = 224

    batched_input = torch.stack(
        (norm_im[0, :, :H, :W, :], norm_im[0, :, H:2*H, W:2*W, :]))
    batched_target = torch.stack(
        (norm_im[0, :, :H, :W, :], uniform_noise[0, :, H:2*H, W:2*W, :]))

    mean_ssim, per_clip_mean_ssim = SSIM(batched_input, batched_target)

    assert mean_ssim.shape == tuple(), "Mean SSIM should be a singleton"
    assert per_clip_mean_ssim.shape == (
        2,), "Per-clip SSIM should have length equal to batch size"

    assert torch.allclose(mean_ssim,
                          torch.tensor(0.5, dtype=torch.float32),
                          atol=1e-1), "Mean SSIM for self & noise should be close to 0.5"
    assert torch.allclose(per_clip_mean_ssim[0],
                          torch.tensor(1.0, dtype=torch.float32),
                          atol=1e-8), "SSIM for self should be 1"
    assert torch.allclose(per_clip_mean_ssim[1],
                          torch.tensor(0.0, dtype=torch.float32),
                          atol=1e-1), "SSIM for noise should be close to 0"


def test_SSIM_on_sequence():
    orig_im, torch_orig_im = rgb_to_byte_tensor(
        "trang-doan-acai-bowl-3118421.jpg")

    norm_im = torch_orig_im.float().div(255.0).view((1, 1, *torch_orig_im.shape))
    uniform_noise = torch.rand_like(norm_im, dtype=torch.float32)

    H = 224
    W = 224

    batched_input = torch.stack(
        (norm_im[:, 0, :H, :W, :], norm_im[:, 0, H:2*H, W:2*W, :]), dim=1)
    batched_target = torch.stack(
        (norm_im[:, 0, :H, :W, :], uniform_noise[:, 0, H:2*H, W:2*W, :]), dim=1)

    mean_ssim, per_clip_mean_ssim = SSIM(batched_input, batched_target)

    assert mean_ssim.shape == tuple(), "Mean SSIM should be a singleton"
    assert per_clip_mean_ssim.shape == (
        1,), "Per-clip SSIM should have length equal to batch size"

    assert torch.allclose(mean_ssim,
                          torch.tensor(0.5, dtype=torch.float32),
                          atol=1e-1), "Mean SSIM for self & noise should be close to 0.5"
    assert torch.allclose(per_clip_mean_ssim,
                          torch.tensor([0.5], dtype=torch.float32),
                          atol=1e-1), "SSIM for self & noise should be close to 0.5"
