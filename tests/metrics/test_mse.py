import pytest
import torch
from acai_vp.metrics.mse import *


def test_MSE():
    with pytest.raises(ValueError):
        assert MSE(
            torch.tensor([0, 1, 7, 3, 4], dtype=torch.float32),
            torch.tensor([5, 6, 2, 8, 9], dtype=torch.float32)
        )

    overall, per_clip = MSE(
        torch.tensor([[[[[0, 1, 7, 3, 4]]]]], dtype=torch.float32),
        torch.tensor([[[[[5, 6, 2, 8, 9]]]]], dtype=torch.float32)
    )
    assert torch.allclose(overall, torch.tensor(25, dtype=torch.float32))
    assert torch.allclose(per_clip, torch.tensor([25], dtype=torch.float32))


def test_PSNR():
    overall, per_clip = PSNR(
        torch.tensor([[[[[1, 2, 1], [2, 3, 6], [5, 8, 2]]]]],
                     dtype=torch.float32) / 255.0,
        torch.tensor([[[[[1, 3, 1], [2, 3, 5], [5, 8, 1]]]]],
                     dtype=torch.float32) / 255.0,
    )

    assert torch.allclose(overall, torch.tensor(52.9020))
    assert torch.allclose(per_clip, torch.tensor([52.9020]))
