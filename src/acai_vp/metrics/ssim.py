"""Structural Similarity and derived/related metrics
"""
import math
import torch

from acai_vp.data.transforms.luma import RGBToLuma
from acai_vp.utils.operations.gaussian_filter import get_gaussian_kernel
from acai_vp.metrics import minimize, maximize


@maximize
def block_SSIM(input_seq, target_seq, window=8):
    """Structural Similarity using sliding square windows.

    Uses simplified formula that assumes alpha = beta = gamma = 1.
    Assumes dynamic range L = 1 (normalized input).

    Args:
        input_seq (torch.Tensor): BxTxHxWxC
        target_seq (torch.Tensor): BxTxHxWxC
        window (int, optional): Defaults to 8.
    """

    if input_seq.shape[-1] > 1:
        converter = RGBToLuma()
        input_seq = converter(input_seq)
        target_seq = converter(target_seq)

    # BxTxHxWxC (C=1) -> (B*T)xCxHxW; no need to permute since there's only single channel
    batched_images_shape = (-1, 1, *input_seq.shape[-3:-1])

    input_seq_windows = torch.nn.functional.unfold(
        input_seq.view(batched_images_shape), kernel_size=window)  # (B*T)x(C*window*window)xL

    target_seq_windows = torch.nn.functional.unfold(
        target_seq.view(batched_images_shape), kernel_size=window)  # (B*T)x(C*window*window)xL

    ssim_windows = per_square_window_SSIM(
        input_seq_windows, target_seq_windows)  # (B*T)xL

    per_clip_mean_ssim = torch.mean(
        ssim_windows.view((input_seq.shape[0], -1)), dim=1)

    return torch.mean(per_clip_mean_ssim), per_clip_mean_ssim


def per_square_window_SSIM(input_seq, target_seq, L=1.0, k1=0.01, k2=0.03):
    """Calculate 'block' SSIM in a single square window

    Args:
        input_seq (torch.Tensor): Bx(C*N*N)xL
        target_seq (torch.Tensor): Bx(C*N*N)xL
    """

    (input_var, input_mean) = torch.var_mean(input_seq, dim=1)
    (target_var, target_mean) = torch.var_mean(target_seq, dim=1)

    input_seq_m = (
        input_seq - input_mean.view(input_seq.shape[0], 1, *input_seq.shape[2:]))
    target_seq_m = (
        target_seq - target_mean.view(target_seq.shape[0], 1, *target_seq.shape[2:]))

    input_target_cov = torch.sum(
        input_seq_m*target_seq_m, dim=1) / (input_seq.shape[1] - 1)

    c1 = (k1*L) ** 2
    c2 = (k2*L) ** 2

    ssim = (2 * input_mean * target_mean + c1) * (2 * input_target_cov + c2) / \
        ((input_mean ** 2 + target_mean ** 2 + c1) * (input_var + target_var + c2))

    return ssim


@maximize
def SSIM(input_seq, target_seq, window=11):
    """Structural Similarity using sliding gaussian windows.

    Uses simplified formula that assumes alpha = beta = gamma = 1.
    Assumes dynamic range L = 1 (normalized input).

    Args:
        input_seq (torch.Tensor): BxTxHxWxC
        target_seq (torch.Tensor): BxTxHxWxC
        window (int, optional): Defaults to 11.
    """

    if input_seq.shape[-1] > 1:
        converter = RGBToLuma()
        input_seq = converter(input_seq)
        target_seq = converter(target_seq)

    # BxTxHxWxC (C=1) -> (B*T)xCxHxW; no need to permute since there's only single channel
    batched_images_shape = (-1, 1, *input_seq.shape[-3:-1])

    ssim_windows = per_gaussian_window_SSIM(
        input_seq.reshape(batched_images_shape),
        target_seq.reshape(batched_images_shape),
        kernel_size=window
    )  # (B*T)xL

    per_clip_mean_ssim = torch.mean(
        ssim_windows.view((input_seq.shape[0], -1)), dim=1)

    return torch.mean(per_clip_mean_ssim), per_clip_mean_ssim


def per_gaussian_window_SSIM(input_seq, target_seq, L=1.0, k1=0.01, k2=0.03, kernel_size=11):
    """Calculate SSIM in a single gaussian window

    Based on https://www.cns.nyu.edu/~lcv/ssim/ssim_index.m

    Args:
        input_seq (torch.Tensor): (B*T)xCxHxW
        target_seq (torch.Tensor): (B*T)xCxHxW
    """

    gaussian_filter = get_gaussian_kernel(
        kernel_size=kernel_size, sigma=1.5, channels=1, device=input_seq.device)

    input_mean = gaussian_filter(input_seq)
    target_mean = gaussian_filter(target_seq)

    input_mean_sq = input_mean**2
    target_mean_sq = target_mean**2
    input_target_mean = input_mean * target_mean

    input_var = gaussian_filter(input_seq ** 2) - input_mean_sq
    target_var = gaussian_filter(target_seq ** 2) - target_mean_sq
    input_target_cov = gaussian_filter(
        input_seq * target_seq) - input_target_mean

    c1 = (k1*L) ** 2
    c2 = (k2*L) ** 2

    ssim = (2 * input_target_mean + c1) * (2 * input_target_cov + c2) / \
        ((input_mean_sq + target_mean_sq + c1) * (input_var + target_var + c2))

    return ssim
