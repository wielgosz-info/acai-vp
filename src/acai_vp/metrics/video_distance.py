"""Video Distance metrics
"""


def FVD(input_seq, target_seq):
    """Fréchet Video Distance

    Args:
        input_seq (torch.Tensor): [description]
        target_seq (torch.Tensor): [description]
    """
    pass


def KVD(input_seq, target_seq):
    """Kernel Video Distance

    Args:
        input_seq (torch.Tensor): [description]
        target_seq (torch.Tensor): [description]
    """
    pass
