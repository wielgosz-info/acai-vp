def minimize(func):
    func.minimize = True
    func.initial = float('Inf')
    return func


def maximize(func):
    func.minimize = False
    func.initial = float('-Inf')
    return func
