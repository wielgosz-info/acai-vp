"""Mean Squared Error and derived/related metrics
"""

import torch
from acai_vp.metrics import minimize, maximize


@minimize
def MSE(input_seq, target_seq):
    """Mean Squared Error

    Args:
        input_seq (torch.Tensor): [description]
        target_seq (torch.Tensor): [description]

    Returns:
        (torch.Tensor, torch.Tensor): overall/per clip Mean Squared Error
    """
    if input_seq.dim() < 5:
        raise ValueError('Input should be a 5D tensor (BxTxHxWxC)')
    if target_seq.dim() < 5:
        raise ValueError('Target should be a 5D tensor (BxTxHxWxC)')
    per_clip = torch.mean((input_seq - target_seq) ** 2, dim=(-4, -3, -2, -1))
    return torch.mean(per_clip), per_clip


@maximize
def PSNR(input_seq, target_seq):
    """Peak Signal-to-Noise Ratio

    Args:
        input_seq (torch.Tensor): [description]
        target_seq (torch.Tensor): [description]

    Returns:
        (torch.Tensor, torch.Tensor): overall/per clip Peak Signal-to-Noise Ratio
    """
    _, per_clip_mse = MSE(input_seq, target_seq)
    per_clip = 10 * torch.log10(1 / per_clip_mse)
    return torch.mean(per_clip), per_clip
