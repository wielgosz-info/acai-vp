class DummyVisdom(object):
    def __getattr__(self, name):
        def func(*args, **kwargs):
            return True
        return func
