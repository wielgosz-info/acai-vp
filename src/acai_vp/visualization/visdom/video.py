import math
import torch


def batch_as_video(viz, batch, env=None, title=None):
    # BxTxHxWxC -> Tx(rows x H)x(per_row x W)x3
    (B, T, H, W, C) = batch.shape
    rows = int(2**math.floor(math.log2(B)/2.0))
    per_row = int(B / rows)
    tmp = batch.transpose(0, 1).reshape((T, B*H, W, C))
    video = torch.cat([tmp[:, r*rows*H:(r+1)*rows*H]
                       for r in range(per_row)], dim=2)
    if C == 1:
        video = video.repeat_interleave(3, 3)
    viz.video(video, opts=dict(
        title=title,
        width=per_row*W + 20,
        height=rows*H + 20
    ), env=env)
