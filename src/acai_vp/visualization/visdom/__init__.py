import logging
from .dummy_visdom import DummyVisdom


def get_visdom(visdom=True):
    if not visdom:
        viz = DummyVisdom()
    else:
        try:
            from visdom import Visdom
            viz = Visdom()

            current_envs = viz.get_env_list()
            desired_envs = ['train', 'validate', 'test']
            for eid in desired_envs:
                if eid not in current_envs:
                    viz.fork_env('main', eid)
        except ImportError:
            logging.getLogger(__name__).warning(
                'visdom not available, disabling')
            viz = DummyVisdom()

    if not viz.check_connection(timeout_seconds=3):
        logging.getLogger(__name__).warning(
            'visdom server not responsive, disabling')
        viz = DummyVisdom()

    return viz
