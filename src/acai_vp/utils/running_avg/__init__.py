class RunningAvg(object):
    def __init__(self, keys=None):
        super().__init__()

        if keys is None:
            keys = ['default']

        self.__count = 0
        self.__avg = {k: 0 for k in keys}

    def append(self, value):
        if isinstance(value, dict):
            for k in value.keys():
                self.__avg[k] = ((self.__count * self.__avg[k]) +
                                 value[k]) / (self.__count + 1)
        else:
            self.__avg['default'] = ((self.__count * self.__avg['default']) +
                                     value) / (self.__count + 1)
        self.__count += 1

    @property
    def avg(self):
        return self.__avg
