import argparse
import os
import math


def readable_dir(x):
    if not os.path.exists(x):
        argparse.ArgumentTypeError('%s does not exist!' % (x,))
    if not os.path.isdir(x):
        argparse.ArgumentTypeError('%s is not a directory!' % (x,))
    if not os.access(x, os.R_OK):
        argparse.ArgumentTypeError('Cannot open %s!' % (x,))
    return x


def writable_dir(x):
    if not os.path.exists(x):
        try:
            os.mkdir(x, 0o700)
        except OSError:
            argparse.ArgumentTypeError('Cannot create %s!' % (x,))
    if not os.path.isdir(x):
        argparse.ArgumentTypeError('%s is not a directory!' % (x,))
    if not os.access(x, os.W_OK):
        # TODO: This check doesn't seem to be working; try to touch & delete?
        argparse.ArgumentTypeError('Cannot write to %s!' % (x,))
    return x


def power_of_two(x):
    x = int(x)
    p = int(math.log2(x))
    if 2**p != x:
        argparse.ArgumentTypeError('%s is not a power of 2!' % (x,))
    return x
