import logging
import sys


def setup_logging(loglevel, id=None):
    """Setup basic logging

    Args:
      loglevel (int): minimum loglevel for emitting messages
    """
    id_str = "[#{}]".format(id) if id is not None else ""
    logformat = id_str + "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    logging.basicConfig(level=loglevel, stream=sys.stdout,
                        format=logformat, datefmt="%Y-%m-%d %H:%M:%S")
