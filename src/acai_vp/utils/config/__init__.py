import yaml
import importlib
import six
from collections.abc import Mapping

try:
    from yaml import CSafeLoader as SafeLoader
except ImportError:
    from yaml import SafeLoader


class Config(Mapping):
    def __init__(self, config_file, data_dir):
        super().__init__()

        self.__root = data_dir
        self.__data = yaml.load(config_file, Loader=SafeLoader)

    def __getitem__(self, key):
        return self.__data[key]

    def __iter__(self):
        return iter(self.__data)

    def __len__(self):
        return len(self.__data)

    @property
    def data_root(self):
        return self.__root

    def get_definition(self, key, secondary_key='klass', default=None):
        value = self[key][secondary_key]
        definitions = []

        if isinstance(value, list):
            paths = value
            unwrap = False
        else:
            paths = [value]
            unwrap = True

        for path in paths:
            if path is None:
                return default

            if not isinstance(path, six.string_types):
                raise ValueError(
                    'The path should be specified as a string. Received {}'.format(path))

            pkg_index = path.rfind('.')

            if pkg_index == -1:
                raise ValueError(
                    'Invalid {} path. Please specify the module path.'.format(key))

            definition = path[pkg_index + 1:]
            pkg = path[:pkg_index]

            imported_module = importlib.import_module(pkg)
            definitions.append(getattr(imported_module, definition))

        if unwrap:
            return definitions[0]
        else:
            return definitions
