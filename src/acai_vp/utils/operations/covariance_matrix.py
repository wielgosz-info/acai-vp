import torch


def covariance_matrix(x, y, x_mean=None, y_mean=None):
    """Simplified covariance matrix

    Args:
        x (torch.Tensor): [description]
        y (torch.Tensor): [description]
        x_mean (torch.Tensor, optional): mean of x. Defaults to None.
        y_mean (torch.Tensor, optional): mean of y. Defaults to None.

    Returns:
        torch.Tensor: [description]
    """

    if x_mean is None:
        x_mean = torch.mean(x)
    if y_mean is None:
        y_mean = torch.mean(y)

    m = torch.stack(((x - x_mean).flatten(), (y - y_mean).flatten()), axis=0)

    return torch.matmul(m, m.t()) / (x.numel() - 1)
