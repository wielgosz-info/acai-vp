# taken from here: https://github.com/ndrplz/ConvLSTM_pytorch
import torch.nn as nn
from torch.autograd import Variable
import torch
import logging
from acai_vp.models import VideoPredictionModel


class ConvLSTMCell(nn.Module):

    def __init__(self, input_size, input_dim, hidden_dim, kernel_size, bias):
        """
        Initialize ConvLSTM cell.

        Parameters
        ----------
        input_size: (int, int)
            Height and width of input tensor as (height, width).
        input_dim: int
            Number of channels of input tensor.
        hidden_dim: int
            Number of channels of hidden state.
        kernel_size: (int, int)
            Size of the convolutional kernel.
        bias: bool
            Whether or not to add the bias.
        """

        super(ConvLSTMCell, self).__init__()

        self.height, self.width = input_size
        self.input_dim = input_dim
        self.hidden_dim = hidden_dim

        self.kernel_size = kernel_size
        self.padding = kernel_size[0] // 2, kernel_size[1] // 2
        self.bias = bias

        self.conv = nn.Conv2d(in_channels=self.input_dim + self.hidden_dim,
                              out_channels=4 * self.hidden_dim,
                              kernel_size=self.kernel_size,
                              padding=self.padding,
                              bias=self.bias)

    def forward(self, input_tensor, cur_state):
        h_cur, c_cur = cur_state

        # concatenate along channel axis
        combined = torch.cat([input_tensor, h_cur], dim=1)

        combined_conv = self.conv(combined)
        cc_i, cc_f, cc_o, cc_g = torch.split(
            combined_conv, self.hidden_dim, dim=1)
        i = torch.sigmoid(cc_i)
        f = torch.sigmoid(cc_f)
        o = torch.sigmoid(cc_o)
        g = torch.tanh(cc_g)

        c_next = f * c_cur + i * g
        h_next = o * torch.tanh(c_next)

        return h_next, c_next

    def init_hidden(self, batch_size, device):
        return (Variable(torch.zeros(batch_size, self.hidden_dim, self.height, self.width)).to(device),
                Variable(torch.zeros(batch_size, self.hidden_dim, self.height, self.width)).to(device))


class ConvLSTM(nn.Module):

    def __init__(self, input_size, input_dim, hidden_dim, kernel_size, num_layers,
                 batch_first=False, bias=True, return_all_layers=False):
        super(ConvLSTM, self).__init__()

        self._check_kernel_size_consistency(kernel_size)

        # Make sure that both `kernel_size` and `hidden_dim` are lists having len == num_layers
        kernel_size = self._extend_for_multilayer(kernel_size, num_layers)
        hidden_dim = self._extend_for_multilayer(hidden_dim, num_layers)
        if not len(kernel_size) == len(hidden_dim) == num_layers:
            raise ValueError('Inconsistent list length.')

        self.height, self.width = input_size

        self.input_dim = input_dim
        self.hidden_dim = hidden_dim
        self.kernel_size = kernel_size
        self.num_layers = num_layers
        self.batch_first = batch_first
        self.bias = bias
        self.return_all_layers = return_all_layers

        cell_list = []
        for i in range(0, self.num_layers):
            cur_input_dim = self.input_dim if i == 0 else self.hidden_dim[i-1]

            cell_list.append(ConvLSTMCell(input_size=(self.height, self.width),
                                          input_dim=cur_input_dim,
                                          hidden_dim=self.hidden_dim[i],
                                          kernel_size=self.kernel_size[i],
                                          bias=self.bias))

        self.cell_list = nn.ModuleList(cell_list)

    def forward(self, input_tensor, hidden_state=None):
        """

        Parameters
        ----------
        input_tensor: todo 
            5-D Tensor either of shape (t, b, c, h, w) or (b, t, c, h, w)
        hidden_state: todo
            zip(layer_output_list, last_state_list) or None

        Returns
        -------
        last_state_list, layer_output
        """

        if not self.batch_first:
            # (t, b, c, h, w) -> (b, t, c, h, w)
            input_tensor = input_tensor.permute(1, 0, 2, 3, 4)

        if hidden_state is None:
            hidden_state = self._init_hidden(
                batch_size=input_tensor.size(0), device=input_tensor.device)

        layer_output_list = []
        last_state_list = []

        seq_len = input_tensor.size(1)
        cur_layer_input = input_tensor

        for layer_idx in range(self.num_layers):

            h, c = hidden_state[layer_idx]
            output_inner = []
            for t in range(seq_len):

                h, c = self.cell_list[layer_idx](input_tensor=cur_layer_input[:, t, :, :, :],
                                                 cur_state=[h, c])
                output_inner.append(h)

            layer_output = torch.stack(output_inner, dim=1)
            cur_layer_input = layer_output

            layer_output_list.append(layer_output)
            last_state_list.append([h, c])

        if not self.return_all_layers:
            layer_output_list = layer_output_list[-1:]
            last_state_list = last_state_list[-1:]

        return layer_output_list, last_state_list

    def _init_hidden(self, batch_size, device):
        init_states = []
        for i in range(self.num_layers):
            init_states.append(
                self.cell_list[i].init_hidden(batch_size, device))
        return init_states

    @staticmethod
    def _check_kernel_size_consistency(kernel_size):
        if not (isinstance(kernel_size, tuple) or
                (isinstance(kernel_size, list) and all([isinstance(elem, tuple) for elem in kernel_size]))):
            raise ValueError('`kernel_size` must be tuple or list of tuples')

    @staticmethod
    def _extend_for_multilayer(param, num_layers):
        if not isinstance(param, list):
            param = [param] * num_layers
        return param


class CrossEntropyLossVideo(torch.nn.CrossEntropyLoss):
    def forward(self, input, target):
        return sum([super(CrossEntropyLossVideo, self).forward(
            input[:, i, ...].permute(0, 3, 1, 2),  # BxHxWxC -> BxCxHxW
            target[:, i, ..., 0].long()
        ) for i in range(input.shape[1])])


class ConvLSTMModel(VideoPredictionModel):
    Loss = CrossEntropyLossVideo
    ConsumedDevices = 3

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        (B, T, H, W, C) = self.input_size

        hidden_dim = [128, 64, 64]
        self.out_channels = 256

        self.encoder = ConvLSTM(
            input_size=(H, W),
            input_dim=C,
            hidden_dim=hidden_dim,
            kernel_size=(5, 5),
            num_layers=3,
            batch_first=True,
            bias=True,
            return_all_layers=True)
        self.decoder = ConvLSTM(
            input_size=(H, W),
            input_dim=C,
            hidden_dim=hidden_dim,
            kernel_size=(5, 5),
            num_layers=3,
            batch_first=True,
            bias=True,
            return_all_layers=True)
        self.conv = nn.Conv2d(in_channels=sum(hidden_dim),
                              out_channels=self.out_channels,
                              kernel_size=(1, 1))

    def distribute_(self, *args, **kwargs):
        super().distribute_(*args, **kwargs)
        self.encoder.to(self.devices[0])
        self.decoder.to(self.devices[1])
        self.conv.to(self.devices[2])

    def forward(self, x):
        # BxTxHxWxC -> BxTxCxHxW
        x = x.permute(0, 1, 4, 2, 3).to(self.devices[0])
        outputs, states = self.encoder(x)

        zeros = torch.zeros_like(x, device=self.devices[1])
        decoded_outputs, _ = self.decoder(zeros, [
            (h.to(self.devices[1]), c.to(self.devices[1]))
            for h, c in states
        ])

        c = torch.cat(decoded_outputs, dim=2).to(self.devices[2])
        o = self.conv(
            c.view((-1, *c.shape[2:]))
        ).view((*c.shape[0:2], -1, *c.shape[-2:]))
        # BxTxCxHxW -> BxTxHxWxC
        return o.permute(0, 1, 3, 4, 2)
