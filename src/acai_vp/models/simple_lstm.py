import torch
import numpy as np

from acai_vp.models import VideoPredictionModel


class SimpleLSTMModel(VideoPredictionModel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.flattened_input_size = int(np.prod(self.input_size[2:]))
        self.flattened_output_size = int(np.prod(self.output_size[2:]))

        self.lstm = torch.nn.LSTM(
            input_size=self.flattened_input_size,
            hidden_size=self.flattened_output_size,
            num_layers=3,
            batch_first=True
        )

    def distribute_(self, *args, **kwargs):
        super().distribute_(*args, **kwargs)
        self.lstm.to(self.devices[0])

    def forward(self, x):
        self.lstm.flatten_parameters()

        flattened_x = x.view(
            (-1, self.input_size[1], self.flattened_input_size)).to(self.devices[0])
        flattened_y, _ = self.lstm(flattened_x)
        return flattened_y.view((-1, *self.output_size[1:]))
