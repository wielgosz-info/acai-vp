import torch
import logging


class VideoPredictionModel(torch.nn.Module):
    Loss = torch.nn.MSELoss
    Optimizer = torch.optim.SGD
    Scheduler = torch.optim.lr_scheduler.StepLR
    ConsumedDevices = 1

    def __init__(self, input_size, output_size):
        super().__init__()

        self.input_size = input_size
        self.output_size = output_size

    def distribute_(self, devices):
        assert len(devices) == self.ConsumedDevices, "This model needs {} devices, {} were provided".format(
            self.ConsumedDevices, len(devices))

        self.devices = devices
        logging.getLogger(__name__).debug(
            'Model is using devices: {}'.format([
                '{}:{}'.format(d.type, d.index)
                for d in self.devices
            ]))

    @property
    def output_device(self):
        return self.devices[-1]
