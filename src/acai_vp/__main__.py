# -*- coding: utf-8 -*-

import torch
from acai_vp.utils.config import Config
from acai_vp.utils.arguments.types import readable_dir, writable_dir
from acai_vp.wrappers import process_run
from acai_vp.wrappers.video_prediction import VideoPredictionWrapper
from acai_vp import __version__
import argparse
import os
import sys
import logging
import faulthandler
faulthandler.enable()


__author__ = "Maciej Wielgosz"
__copyright__ = "Maciej Wielgosz"
__license__ = "mit"


def parse_args(args):
    """Parse command line parameters

    Args:
      args ([str]): command line parameters as list of strings

    Returns:
      :obj:`argparse.Namespace`: command line parameters namespace
    """
    parser = argparse.ArgumentParser(
        description="Video Prediction in PyTorch")

    parser.add_argument(
        "--version",
        action="version",
        version="acai-vp {ver}".format(ver=__version__))
    parser.add_argument(
        "-v",
        "--verbose",
        dest="loglevel",
        help="set loglevel to INFO",
        action="store_const",
        const=logging.INFO)
    parser.add_argument(
        "-vv",
        "--very-verbose",
        dest="loglevel",
        help="set loglevel to DEBUG",
        action="store_const",
        const=logging.DEBUG)

    parser.add_argument(
        "-d",
        "--data-dir",
        type=readable_dir,
        required=True)
    parser.add_argument(
        "-o",
        "--out-dir",
        type=writable_dir,
        required=True)
    # TODO: Create per run subdir, add some flag to enable this?
    parser.add_argument(
        '-c',
        '--config-file',
        type=argparse.FileType('r'),
        nargs='?',
        default=sys.stdin
    )

    parser.add_argument(
        '-r',
        '--resume-checkpoint',
        type=argparse.FileType('r')
    )

    parser.add_argument(
        '--no-visdom',
        action='store_false',
        dest='visdom'
    )

    parser.add_argument(
        '--world-size',
        type=int,
        default=-1,
        help='How many processes will be run on ALL nodes. Should take into account GPUs available on each node and Model.ConsumedDevices value. -1 means "this is the only node, calculate appropriate world size".'
    )

    parser.add_argument(
        '--start-rank',
        type=int,
        default=0,
        help='Which rank this node starts assigning ranks from.'
    )

    parser.add_argument(
        '--master-addr',
        type=str,
        default='localhost'
    )

    parser.add_argument(
        '--master-port',
        type=str,
        default='12355'
    )

    return parser.parse_args(args)


def main(args):
    """Main entry point allowing external calls

    Args:
        args ([str]): command line parameter list
    """
    args = parse_args(args)

    os.environ['MASTER_ADDR'] = args.master_addr
    os.environ['MASTER_PORT'] = args.master_port
    os.environ['NCCL_BLOCKING_WAIT'] = '1'

    config = Config(args.config_file, os.getcwd())
    device_type, _, nprocs = VideoPredictionWrapper.get_node_capabilities_for_model(
        config,
        config.get_definition('model'),
        logging.getLogger(__name__)
    )

    args.config_file.close()
    args.config_file = args.config_file.name

    if args.resume_checkpoint is not None:
        args.resume_checkpoint.close()
        args.resume_checkpoint = args.resume_checkpoint.name

    torch.multiprocessing.spawn(
        process_run,
        args=(device_type, nprocs, args),
        nprocs=nprocs,
        join=True
    )


def run():
    """Entry point for console_scripts
    """
    main(sys.argv[1:])


if __name__ == "__main__":
    run()
