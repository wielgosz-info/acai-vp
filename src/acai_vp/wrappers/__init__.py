import torch

from acai_vp.utils.arguments import setup_logging
from acai_vp.wrappers.video_prediction import VideoPredictionWrapper


def process_run(node_rank, device_type, nprocs, args):
    rank = args.start_rank + node_rank
    world_size = args.world_size if args.world_size > -1 else nprocs

    torch.distributed.init_process_group(
        'nccl' if device_type == 'cuda' else 'gloo',
        rank=rank,
        world_size=world_size
    )

    setup_logging(args.loglevel, rank)

    wrapper = VideoPredictionWrapper(
        config_file=open(args.config_file),
        data_dir=args.data_dir,
        out_dir=args.out_dir,
        visdom=args.visdom,
        resume_checkpoint=open(
            args.resume_checkpoint) if args.resume_checkpoint else None,
        start_rank=args.start_rank,
        node_rank=node_rank,
        world_size=world_size
    )
    wrapper.run()

    torch.distributed.destroy_process_group()
