import itertools
import logging
import math
import os
import pprint
import shutil
import sys
import tempfile

import torch
import torchvision
from tqdm import tqdm

from acai_vp.utils.config import Config
from acai_vp.utils.running_avg import RunningAvg
from acai_vp.visualization.visdom import get_visdom
from acai_vp.visualization.visdom.video import batch_as_video


class VideoPredictionWrapper(object):
    def __init__(
        self,
        config_file,
        data_dir,
        out_dir,
        visdom,
        resume_checkpoint=None,
        start_rank=0,
        node_rank=0,
        world_size=-1
    ):
        super().__init__()

        self.__logger = logging.getLogger(__name__)
        self.__config = Config(config_file, data_dir)
        self.__out_dir = out_dir
        self.__viz = get_visdom(visdom)
        self.__resume_checkpoint = resume_checkpoint
        self.__start_rank = start_rank
        self.__node_rank = node_rank

        self.Model = self.__config.get_definition('model')

        device_type, device_ids, nprocs = VideoPredictionWrapper.get_node_capabilities_for_model(
            self.__config,
            self.Model,
            self.__logger
        )

        self.__device_type = device_type
        self.__device_ids = device_ids
        self.__nprocs = nprocs
        self.__world_size = world_size if world_size > 0 else self.__nprocs

        # find out which devices we can use
        self.__rank_devices = self.__device_ids[self.__node_rank*self.Model.ConsumedDevices:(
            self.__node_rank+1)*self.Model.ConsumedDevices]

        # set default cuda device just in case
        if self.__device_type == 'cuda':
            torch.cuda.set_device(self.__rank_devices[0])

        Dataset = self.__config.get_definition('dataset')
        Transform = self.__config.get_definition('dataset', 'transform')
        TargetTransform = self.__config.get_definition(
            'dataset', 'target_transform')
        MetricsTransform = self.__config.get_definition(
            'dataset', 'metrics_transform')
        DisplayTransform = self.__config.get_definition(
            'dataset', 'display_transform')
        OutputTransform = self.__config.get_definition(
            'model', 'output_transform')

        self.__output_transform = OutputTransform(
        ) if OutputTransform is not None else lambda x: x
        self.__display_transform = DisplayTransform(
        ) if DisplayTransform is not None else lambda x: x
        self.__metrics_transform = MetricsTransform(
        ) if MetricsTransform is not None else lambda x: x

        transforms = torchvision.datasets.vision.StandardTransform(
            transform=Transform() if Transform is not None else None,
            target_transform=TargetTransform() if TargetTransform is not None else None
        )

        self.__metrics = dict(zip(self.__config['validation']['metrics'],
                                  self.__config.get_definition('validation', 'metrics')))
        self.__primary_metric = self.__config['validation']['primary_metric']
        self.__loss_metric = self.Model.Loss.__name__

        self.__epochs = self.__config['training']['epochs']
        self.__start_epoch = 0
        self.__current_epoch = None
        self.__batch_size = self.__config['training']['batch_size']
        self.__best_val = self.__metrics[self.__primary_metric].initial

        tmp_loader = torch.utils.data.DataLoader(
            Dataset(root=self.__config.data_root, test=False,
                    end=self.__batch_size, transforms=transforms),
            batch_size=self.__batch_size
        )
        tmp_batch = next(iter(tmp_loader))

        self.__input_size = tmp_batch[0].size()
        self.__output_size = tmp_batch[1].size()

        del tmp_batch

        self.__train_loader = self.get_loader(Dataset(
            root=self.__config.data_root,
            test=False,
            end=self.__config['dataset']['train_size'],
            transforms=transforms
        ))
        self.__val_loader = self.get_loader(Dataset(
            root=self.__config.data_root,
            test=False,
            end=self.__config['dataset']['val_size'],
            transforms=transforms
        ))
        self.__test_loader = self.get_loader(Dataset(
            root=self.__config.data_root,
            test=True,
            end=self.__config['dataset']['test_size'],
            transforms=transforms
        ))

    @property
    def nprocs(self):
        """The number of available processes on this node when running specified Model"""
        return self.__nprocs

    @staticmethod
    def get_node_capabilities_for_model(config, Model, logger):
        device_ids = [0] * Model.ConsumedDevices
        device_type = 'cpu'
        nprocs = 1
        gpus = config.get('setup', {}).get('gpus', -1)

        if not torch.cuda.is_available():
            gpus = 0
        elif gpus == -1:
            gpus = torch.cuda.device_count()

        if gpus > 0:
            device_type = 'cuda'
            device_ids = list(range(gpus))

            if Model.ConsumedDevices > gpus:
                repeats = math.ceil(Model.ConsumedDevices /
                                    gpus)
                logger.warn(
                    'Number of available devices ({}) is not sufficient for the model requirements ({}). Devices will repeat.'.format(
                        gpus,
                        Model.ConsumedDevices)
                )
                device_ids = list(itertools.chain.from_iterable(
                    itertools.repeat(x, device_ids) for x in device_ids))
                nprocs = 1
            else:
                nprocs = math.floor(gpus / Model.ConsumedDevices)

        return device_type, device_ids, nprocs

    def get_loader(self, dataset):
        return torch.utils.data.DataLoader(
            dataset,
            batch_size=self.__batch_size,
            shuffle=False,
            num_workers=self.__world_size,
            pin_memory=True
        )

    def run(self):
        self.__logger.info('Preparing')

        model = self.Model(self.__input_size, self.__output_size)

        resume_checkpoint = self.__resume_from_checkpoint(model)

        model.distribute_([
            torch.device(self.__device_type, d_id)
            for d_id in self.__rank_devices])

        self.__model_output_device = model.output_device

        ddp_kwargs = {}
        if self.__device_type == 'cuda' and (self.Model.ConsumedDevices == 1 or len(set(self.__rank_devices)) < 2):
            ddp_kwargs['device_ids'] = [self.__rank_devices[0]]
        ddp_model = torch.nn.parallel.DistributedDataParallel(
            model, **ddp_kwargs)

        criterion = self.Model.Loss(
            **self.__config['training'].get('loss', {}))
        optimizer = self.Model.Optimizer(
            ddp_model.parameters(), **self.__config['training'].get('optimizer', {}))
        scheduler = self.Model.Scheduler(
            optimizer, **self.__config['training'].get('scheduler', {})
        )

        if resume_checkpoint is not None:
            optimizer.load_state_dict(
                resume_checkpoint['optimizer_state_dict'], loc=self.__rank_devices[0])

        self.__sync_model_state_dict(ddp_model)

        self.__logger.info('Training starting')

        if self.__node_rank == 0:
            progress_bar = tqdm(
                total=self.__epochs,
                file=sys.stdout,
                unit='epoch',
                ncols=120,
                position=0
            )

        for epoch in range(self.__start_epoch, self.__epochs):
            self.__current_epoch = epoch
            loss = self.train(
                self.__node_rank + 1,
                ddp_model,
                criterion,
                optimizer
            )
            metrics = self.validate(
                self.__node_rank + self.__nprocs + 1,
                ddp_model,
                criterion
            )

            if self.__node_rank == 0:
                progress_bar.set_postfix({
                    'loss': '{:.4f}'.format(loss),
                    'val_loss': '{:.4f}'.format(metrics[self.__loss_metric])
                })

                if self.__metrics[self.__primary_metric].minimize:
                    is_best = metrics[self.__primary_metric] < self.__best_val
                else:
                    is_best = metrics[self.__primary_metric] > self.__best_val
                if is_best:
                    self.__best_val = metrics[self.__primary_metric]

                if self.__start_rank == 0:
                    self.save(ddp_model, optimizer,
                              epoch + 1, is_best, metrics)

                progress_bar.update(1)

            scheduler.step()

        if self.__node_rank == 0:
            progress_bar.close()

        self.__current_epoch = None

        self.__logger.info('Testing starting')
        test_metrics = self.test(self.__node_rank,
                                 ddp_model, criterion)

        if self.__node_rank == 0:
            sys.stdout.flush()
            pprint.pprint(test_metrics)

    def __resume_from_checkpoint(self, model):
        resume_checkpoint = None
        if self.__resume_checkpoint is not None:
            resume_checkpoint = torch.load(self.__resume_checkpoint)
            if self.__node_rank == 0:
                model.load_state_dict(
                    resume_checkpoint['model_state_dict'], loc=torch.device('cpu'))
                self.__start_epoch = resume_checkpoint.get('epoch', 0)
                self.__best_val = resume_checkpoint.get(
                    self.__primary_metric, self.__metrics[self.__primary_metric].initial)
        return resume_checkpoint

    def __sync_model_state_dict(self, ddp_model):
        TMP_CHECKPOINT_PATH = tempfile.gettempdir() + "/model.checkpoint"

        if self.__node_rank == 0:
            torch.save(ddp_model.state_dict(), TMP_CHECKPOINT_PATH)
            self.__logger.debug('Temporary checkpoint saved')

        # Use a barrier() to make sure that process 1 loads the model after process
        # 0 saves it.
        torch.distributed.barrier()

        map_location = {'cuda:{}'.format(x): '{}:{}'.format(self.__device_type, y)
                        for x, y in zip(
            range(self.Model.ConsumedDevices),
            self.__rank_devices
        )}

        self.__logger.debug(
            'Loading temporary checkpoint with map_location={}'.format(map_location))

        ddp_model.load_state_dict(
            torch.load(TMP_CHECKPOINT_PATH, map_location=map_location))

        self.__logger.debug('Temporary checkpoint loaded')

        # Use a barrier() to make sure that all processes have finished reading the
        # checkpoint
        torch.distributed.barrier()

        if self.__node_rank == 0:
            os.remove(TMP_CHECKPOINT_PATH)
            self.__logger.debug('Temporary checkpoint removed')

    def train(self, tqdm_position, model, criterion, optimizer):
        loader = self.__train_loader

        # switch to train mode
        model.train()

        avg_loss = RunningAvg(['loss'])
        total = int(
            math.ceil(len(loader.dataset)/self.__batch_size))

        with tqdm(
            total=total,
            file=sys.stdout,
            unit='batch',
            ncols=120,
            desc='[#{}]'.format(self.__node_rank),
            position=tqdm_position
        ) as progress_bar:
            for input_seq, target_seq in loader:
                optimizer.zero_grad()

                target_seq = target_seq.to(
                    device=self.__model_output_device,
                    non_blocking=True
                )
                output_seq = model(input_seq)

                if self.__model_output_device.type == 'cuda':
                    torch.cuda.synchronize(self.__model_output_device)

                loss = criterion(output_seq, target_seq)
                avg_loss.append({'loss': loss.item()})

                progress_bar.set_postfix(
                    {'loss': '{:.4f}'.format(loss.item())})

                loss.backward()
                optimizer.step()

                progress_bar.update(1)

                # batch_as_video(
                #   self.__viz,
                #   self.__display_transform(self.__output_transform(output_seq)),
                #   env='train',
                #   title='self.Model output')

        return avg_loss.avg['loss']

    def validate(self, tqdm_position, model, criterion):
        return self.eval(tqdm_position, model, criterion, self.__val_loader, viz_env='validate')

    def test(self, tqdm_position, model, criterion):
        return self.eval(tqdm_position, model, criterion, self.__test_loader, viz_env='test')

    def eval(self, tqdm_position, model, criterion, loader, viz_env=None):
        # switch to evaluate mode
        model.eval()

        avg_metrics = RunningAvg(
            list(self.__metrics.keys()) + [self.__loss_metric])
        total = int(math.ceil(len(loader.dataset)/self.__batch_size))

        with torch.no_grad():
            with tqdm(
                total=total,
                file=sys.stdout,
                unit='batch',
                ncols=120,
                desc='[#{}]'.format(self.__node_rank),
                position=tqdm_position
            ) as progress_bar:
                for input_seq, target_seq in loader:
                    target_seq = target_seq.to(
                        device=self.__model_output_device,
                        non_blocking=True
                    )
                    output_seq = model(input_seq)

                    if self.__model_output_device.type == 'cuda':
                        torch.cuda.synchronize(self.__model_output_device)

                    loss = criterion(output_seq, target_seq)

                    transformed_output_seq = self.__output_transform(
                        output_seq)

                    metrics = {
                        k: metric_func(
                            self.__metrics_transform(transformed_output_seq),
                            self.__metrics_transform(target_seq)
                        )[0].item()
                        for k, metric_func in self.__metrics.items()
                    }
                    metrics.update({
                        self.__loss_metric: loss.item()
                    })
                    avg_metrics.append(metrics)

                    progress_bar.set_postfix({
                        'loss': '{:.4f}'.format(loss.item()),
                        self.__primary_metric[self.__primary_metric.rfind(
                            '.')+1:]: '{:.4f}'.format(metrics[self.__primary_metric])
                    })
                    progress_bar.update(1)

                    if viz_env:
                        P = 32
                        interleaved = torch.stack((
                            self.__display_transform(target_seq[:P]),
                            self.__display_transform(
                                transformed_output_seq[:P])
                        ), dim=0).transpose(0, 1).reshape((-1, *target_seq.shape[1:]))
                        batch_as_video(
                            self.__viz,
                            interleaved,
                            viz_env,
                            title="[#{}]{} Original + Predicted".format(
                                self.__node_rank,
                                '[epoch: {}]'.format(
                                    self.__current_epoch) if self.__current_epoch is not None else ''
                            ))
                        viz_env = None

        return avg_metrics.avg

    def save(self, model, optimizer, epoch, is_best, metrics, filename='checkpoint.pth.tar'):
        state = {
            'epoch': epoch,
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
        }
        state.update(metrics)
        torch.save(state, os.path.join(self.__out_dir, filename))

        if is_best:
            shutil.copyfile(os.path.join(self.__out_dir, filename), os.path.join(
                self.__out_dir, 'model_best.pth.tar'))
