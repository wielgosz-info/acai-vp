import torch


class RGBToLuma(object):
    """ITU-R 601-2 luma transform (same as in Pillow/torchvision) but for Tensors instead of PIL images"""

    def __init__(self):
        self.BT_601 = torch.tensor([299/1000, 587/1000, 114/1000],
                                   dtype=torch.float32)

    def __call__(self, input_seq):
        """ITU-R 601-2 luma transform (same as in Pillow/torchvision) but for Tensors instead of PIL images

        Args:
            input_seq (torch.Tensor): BxTxHxWxC batch of RGB video clips

        Returns:
            torch.Tensor: BxTxHxWx1 batch of grayscale video clips
        """
        BT_601 = self.BT_601.to(device=input_seq.device)
        L = torch.sum(torch.mul(input_seq, BT_601),
                      dim=-1, keepdim=True)
        if input_seq.dtype == torch.uint8:
            L.round_()
            L.clamp_(min=0, max=255)
        return L.to(dtype=input_seq.dtype)

    def __repr__(self):
        return self.__class__.__name__ + '()'
