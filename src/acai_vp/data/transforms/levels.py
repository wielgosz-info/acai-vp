import torch


class LevelsToFloat(object):
    """Convert tensor containing raw image data into normalized one.

    Args:
        levels ([int]): Number of color levels in image
    """

    def __init__(self, levels=256):
        self.levels = float(levels - 1)

    def __call__(self, input_seq):
        return (input_seq / self.levels).float()

    def __repr__(self):
        return self.__class__.__name__ + '(levels={})'.format(self.levels)


class FloatToLevels(object):
    """Convert tensor containing normalized image data into raw, uint8 one.

    Args:
        levels ([int]): Number of color levels in image
    """

    def __init__(self, levels=256):
        self.levels = float(levels - 1)

    def __call__(self, input_seq):
        return (input_seq * self.levels).byte()

    def __repr__(self):
        return self.__class__.__name__ + '(levels={})'.format(self.levels)


class ArgmaxToLevels(object):
    def __call__(self, input_seq):
        return torch.argmax(input_seq, dim=-1, keepdim=True).byte()

    def __repr__(self):
        return self.__class__.__name__ + '()'
