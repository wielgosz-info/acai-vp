import os
import math
import numpy as np
import torch
import torchvision
from functools import lru_cache


class MovingMNIST(torch.utils.data.IterableDataset):
    def __init__(self,
                 root,
                 start=0,
                 end=10000,
                 test=False,
                 x_len=10,
                 y_len=10,
                 digit_shape=(28, 28),  # HxW
                 frame_shape=(64, 64),  # HxW
                 n_digits=2,
                 step_length=0.1,
                 digits_file='moving-mnist/train-images-idx3-ubyte.gz',
                 test_file='moving-mnist/mnist_test_seq.npy',
                 transforms=None):
        """Dataset for MovingMNIST (http://www.cs.toronto.edu/~nitish/unsupervised_video/)

        Yields clips as BxTxHxWxC float32 tensors.

        Args:
            start ([int]): indices from (important when num_workers > 0). Defaults to 0.
            end ([int]): indices to (important when num_workers > 0). Defaults to 10000.
            x_len ([int]):  number of input frames. Defaults to 10.
            y_len ([int]): number of output frames. Defaults to 10.
            digit_shape ([tuple(int, int)]): height x width of single MNIST digit. Defaults to (28,28).
            frame_shape ([tuple(int, int)]): height x width of generated MovingMNIST frame. Defaults to (64,64)
            n_digits ([int]): number of digits per frame. Defaults to 2.
            step_length ([float]): digits base speed. Defaults to 0.1.
            test ([bool]): should test file be used (True) or data generated on the fly (False)? Defaults to False.
            root ([str]): data root. Defaults to '/data/moving-mnist-example/'.
            digits_file ([str]): file with MNIST digits (http://yann.lecun.com/exdb/mnist/train-images-idx3-ubyte.gz). Defaults to 'train-images-idx3-ubyte.gz'.
            test_file ([str]): file with test set (for replicability; http://www.cs.toronto.edu/~nitish/unsupervised_video/mnist_test_seq.npy). Defaults to 'mnist_test_seq.npy'.
            transforms ([torchvision.datasets.vision.StandardTransform]): input/target transforms
        """

        super().__init__()

        self.digit_shape = digit_shape
        self.frame_shape = frame_shape
        self.n_digits = n_digits
        self.step_length = step_length

        self.test = test
        self.x_len = x_len
        self.y_len = y_len

        self.test_path = os.path.join(root, test_file)
        self.digits_path = os.path.join(root, digits_file)

        self.transforms = transforms if transforms is not None else torchvision.datasets.vision.StandardTransform()

        if self.test:
            assert x_len + y_len <= 20, 'test set clips have only 20 frames'
            self.digit_shape = (28, 28)
            self.frame_shape = (64, 64)
            self.start = 0
            self.end = min(end, len(self.test_data))
        else:
            self.start = start
            self.end = end

    @property
    @lru_cache(maxsize=1)
    def test_data(self):
        """Loads standard test data

        Returns:
            torch.Tensor: SxTxHxWxC
        """
        if not os.path.exists(self.test_path):
            from urllib.request import urlretrieve
            urlretrieve(
                'http://www.cs.toronto.edu/~nitish/unsupervised_video/mnist_test_seq.npy', self.test_path)
        return torch.from_numpy(np.expand_dims(np.swapaxes(np.load(self.test_path), 0, 1), 4)).byte()

    @property
    @lru_cache(maxsize=1)
    def digits_data(self):
        """Load (and download if needed) MNIST digits

        Returns:
            numpy.ndarray: SxHxWx1
        """
        if not os.path.exists(self.digits_path):
            from urllib.request import urlretrieve
            urlretrieve(
                'http://yann.lecun.com/exdb/mnist/train-images-idx3-ubyte.gz', self.digits_path)
        import gzip
        with gzip.open(self.digits_path, 'rb') as f:
            data = np.frombuffer(f.read(), np.uint8, offset=16)
        return torch.from_numpy(data).view(-1, *self.digit_shape, 1).byte()

    @property
    def digit_image(self):
        """Infinite digits generator

        Yields:
            numpy.ndarray: MNIST digit HxW
        """
        digits_indices = torch.randperm(len(self.digits_data))
        i = 0
        while True:
            yield self.digits_data[digits_indices[i]]
            i = (i + 1) % len(self.digits_data)

    def __len__(self):
        return self.end - self.start

    def __iter__(self):
        worker_info = torch.utils.data.get_worker_info()
        if worker_info is None:  # single-process data loading, return the full iterator
            iter_start = self.start
            iter_end = self.end
        else:  # in a worker process
            # split workload
            per_worker = int(
                math.ceil((self.end - self.start) / float(worker_info.num_workers)))
            worker_id = worker_info.id
            iter_start = self.start + worker_id * per_worker
            iter_end = min(iter_start + per_worker, self.end)
        if self.test:
            return self.test_data_generator(iter_start, iter_end)
        else:
            return self.moving_mnist_generator(iter_start, iter_end)

    def test_data_generator(self, iter_start, iter_end):
        i = iter_start
        while i < iter_end:
            seq_len = self.x_len+self.y_len
            seq = self.test_data[i, :seq_len]
            yield self.transforms(seq[:self.x_len], seq[self.x_len:seq_len])
            i += 1

    def moving_mnist_generator(self, iter_start, iter_end):
        i = iter_start
        while i < iter_end:
            seq_len = self.x_len+self.y_len
            seq = self.moving_mnist_sequence(seq_len)
            yield self.transforms(seq[:self.x_len], seq[self.x_len:seq_len])
            i += 1

    def moving_mnist_sequence(self, length):
        """Creates single Moving MNIST clip with specified length

        copied almost verbatim from code at http://www.cs.toronto.edu/~nitish/unsupervised_video/

        Args:
            length (int): sequence length

        Returns:
            torch.Tensor: TxHxWxC tensor containing the individual frames, as a uint8 tensor
        """

        seq = torch.zeros((length, *self.frame_shape, 1),
                          dtype=torch.uint8)  # TxHxWxC
        size_y = self.frame_shape[0] - self.digit_shape[0]
        size_x = self.frame_shape[1] - self.digit_shape[1]

        for n in range(self.n_digits):
            digit_image = next(self.digit_image)

            # Initial position uniform random inside the box.
            y = torch.rand((1,))
            x = torch.rand((1,))

            # Choose a random velocity.
            theta = torch.rand((1,)) * 2 * math.pi
            v_y = torch.sin(theta)
            v_x = torch.cos(theta)

            start_y = torch.zeros((length, ), dtype=torch.float32)
            start_x = torch.zeros((length, ), dtype=torch.float32)
            for i in range(length):
                # Take a step along velocity.
                y += v_y * self.step_length
                x += v_x * self.step_length

                # Bounce off edges.
                if x <= 0:
                    x = torch.tensor([0.0])
                    v_x = -v_x
                if x >= 1.0:
                    x = torch.tensor([1.0])
                    v_x = -v_x
                if y <= 0:
                    y = torch.tensor([0.0])
                    v_y = -v_y
                if y >= 1.0:
                    y = torch.tensor([1.0])
                    v_y = -v_y

                start_y[i] = y
                start_x[i] = x

            # Scale to the size of the canvas.
            start_y = (size_y * start_y).int()
            start_x = (size_x * start_x).int()

            for i in range(length):
                top = start_y[i]
                left = start_x[i]
                bottom = top + self.digit_shape[0]
                right = left + self.digit_shape[1]
                seq[i, top:bottom, left:right, :] = torch.max(
                    seq[i, top:bottom, left:right, :], digit_image)

        return seq
