FROM nvidia/cuda:10.1-cudnn7-runtime-ubuntu18.04 AS base
RUN apt-get update && apt-get install -y --no-install-recommends \
    python3.6 \
    && rm -rf /var/lib/apt/lists/*

FROM base AS temp
RUN apt-get update && apt-get install -y --no-install-recommends \
    git \
    openssh-client \
    python3-pip \
    python3-setuptools \
    && rm -rf /var/lib/apt/lists/* \
    && python3 -m pip install --no-cache-dir --upgrade \
    pip \
    setuptools \
    torch \
    torchvision \
    wheel

# Install the acai_vp lib itself (note: torch && torchvision pre-installed earlier to leverage docker cache)
# ARG TAG=develop
# RUN python3 -m pip install --no-cache-dir --upgrade git+https://bitbucket.org/wielgosz-info/acai-vp.git@${TAG}#egg=acai_vp
COPY . /tmp/acai-vp
RUN python3 -m pip install --no-cache-dir --upgrade /tmp/acai-vp

# Copy packages over to a new container
FROM base AS target
COPY --from=temp /usr/local/lib/python3.6/dist-packages/ /usr/local/lib/python3.6/dist-packages/

# Setup default acai_vp alias
RUN echo "alias acai_vp='python3.6 -m acai_vp -vv --data-dir /data --out-dir /out --config-file /acai-vp-config.yaml --no-visdom'" >> /etc/bash.bashrc

# Switch user to non-root
ARG UID=1000
ARG GID=1000
ENV USERNAME acai
RUN groupadd -g ${GID} ${USERNAME} && useradd -s /bin/bash -u ${UID} -g ${GID} ${USERNAME}
USER ${USERNAME}:${USERNAME}
VOLUME [ "/data", "/out" ]
WORKDIR /out

ENTRYPOINT ["/bin/bash"]
CMD []
